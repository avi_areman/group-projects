/****************************************************
implementation file for linked list class.
*****************************************************/

#include "list.h"
#include <stdlib.h> //for rand()

namespace csc212
{
	/* listNode stuff: */
	listNode::listNode()
	{
		this->next = 0; //set the next pointer to be null
	}

	listNode::listNode(val_type x, listNode *pNext)
	{
		this->data = x;
		this->next = pNext;
	}

	/* linked list implementation: */
	list::list()
	{
		this->root = 0; //initialize the list to be empty
		//remember 0 == NULL, which is never memory you own.
	}

	list::list(const list &L)
	{
		/* TODO: write this. */
		if (!L.root) {  //special case
			this->root = 0;
			return;
		}
		listNode* p = L.root;
		this->root = new listNode(p->data);
		listNode* q = root;
		p = p->next;
		while (p) {
			q->next = new listNode(p->data);
			p = p->next;
			q = q->next;
		}
	}

	list::~list()
	{
		this->clear(); //delete all nodes, deallocating memory
	}

	list& list::operator=(const list& L)
	{
		/* TODO: write this. */
		listNode* temp = L.root;
		this->clear();
		while (temp != 0) {
			this->insert(temp->data);
			temp = temp->next;  
		}
		return *this;
	}

	bool operator==(const list& L1, const list& L2)
	{
		/* TODO: write this. */
		listNode* p;
		listNode* q;
		p = L1.root;
		q = L2.root;
		while (p != 0 || q != 0) {
			if ((p->data != q->data)) {
				return false;
			}
			p = p->next;
			q = q->next;
		}
		return true;
	}
	bool operator !=(const list& L1, const list& L2)
	{
		return !(L1==L2);
	}

	void list::insert(val_type x)
	{
		/* TODO: write this. */
		listNode* p = root; // P is in the front
		listNode* q = root;
		listNode* newNode= new listNode;
		newNode->data=x;
		if(root == 0)                             	// Makes root not a null pointer.
			root = newNode;
		while (p != 0) {                            // Accounts for when root is no longer null.
            if (root->data >=newNode->data){        // If no value is less than x, insert front, making newNode, root.
				newNode->next = root;
				root = newNode;
				break;
			}
            if (root->next == 0){                   // Maybe? It needs fixing.
                root->next = newNode;
                break;
            }
			if (p->next == 0){
			    p->next = newNode;
				break;
			}
			if(((p->next)->data> newNode->data) && (((q->next)->next) != 0) && (p->next != 0)) {
			    q = q->next;
				p = p->next;
				q -> next = newNode;
                newNode->next = p;
				break;
			}
			q = p;
			p = p->next;
		}
	}

	void list::remove(val_type x)
	{
		/* TODO: write this. */
		listNode* p = root->next; //main one
		listNode* q = root; //previous
		if(root->data == x){
			root = p->next;
			delete p;
			return;
		}
		while(p && p->data != x){
			q = p;
			p = p->next;
			if(p->next != 0){
				return;
			}
		}
		if(p->next != 0){
			q->next = p->next;
		}else{
			q->next = 0;
		}
		delete p;


	}

	bool list::isEmpty()
	{
		return (this->root == 0);
	}

	void list::clear()
	{
		//idea: repeatedly delete the root node...
		listNode* p;
		while((p = root)) //yes, I do mean "=", not "=="
		{
			root = p->next;
			delete p;
		}
	}

	ostream& operator<<(ostream& o, const list& L)
	{
		listNode* p;
		p = L.root;
		while(p)
		{
			o << p->data << " ";
			p = p->next;
		}
		return o;
	}

	bool list::search(val_type x) const
	{
		listNode* p;
		p = root;
		while(p && p->data != x) //again, short circuit evaluation is important...
			p = p->next;
		if(p)
			return true;
		else
			return false;
	}

	unsigned long list::length() const
	{
		/* TODO: write this. */
		listNode* p = root;
		unsigned int i;
		for (i = 0; p != 0; i++) {
			p = p->next;
		}
		return i;
	}

	void list::merge(const list& L1, const list& L2)
	{
		/* TODO: write this. */
		//this algorithm should run in LINEAR TIME and set *this
		//to be the union of L1 and L2, and furthermore the list should remain sorted.
		listNode* p = L1.root;
		listNode* q = L2.root;
		this->clear();
		unsigned long i;
		for (i = 0; p != 0 && q != 0; i++) {
			this->insert(p->data);
			this->insert(q->data);
				p = p->next;
				q = q->next;
		}
	}

	void list::randomFill(unsigned long n, unsigned long k)
	{
		//we want to fill the list with n random integers from 0..k-1
		this->clear(); //reset to the empty list
		unsigned long i;
		for(i=0; i<n; i++)
		{
			this->insert((val_type)(rand()%k));
		}
	}

	void list::intersection(const list &L1, const list& L2)
	{
		/* TODO: write this. */
		//this algorithm should run in LINEAR TIME, setting *this
		//to be the intersection (ordered) of L1 and L2.
		this->clear();
		listNode* p = L1.root;                // Doesn't really work. :(
		listNode* q = L2.root;
		for (p = L1.root; (p != 0 && p->data != 0 && p->next != 0) || (q->data != 0 && q->next != 0); p = p->next) {
			if (p->data == q->data) {
				this->insert(p->data);
				remove(q->data);
			}
		}
	}
}
